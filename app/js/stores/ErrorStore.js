import {action, observable} from 'mobx';
import Firebase from '../utils/Firebase';

class ErrorStore {
    @observable store;

    constructor() {
        this.store = {
            data:      [],
            isLoading: false,
            isLoaded:  false,
            error:     null,
        };
    }

    @action('Login data')
    loadData(date) {
        this.store.isLoading = true;
        this.store.error = null;
        Firebase.getDate(date).then(data => {
            const newData = [];
            data.forEach(item => {
                const value = item.val();
                Object.keys(value).forEach(errorTime =>
                    newData.push({appName: item.key, errorTime, key: Math.random(), ...value[errorTime]}));
            });
            this.store.data = newData;
            this.store.error = null;
            this.store.isLoading = false;
            this.store.isLoaded = true;
        }, err => {
            console.log(333, err);
            this.store.error = err;
            this.store.isLoading = false;
            this.store.isLoaded = true;
        });
        /*setTimeout(_ => {
            for (let i = 0; i < 46; i++) {
                this.store.data.push({
                    key:     i,
                    name:    `Edward King ${i}`,
                    age:     32,
                    address: `London, Park Lane no. ${i}`,
                });
                this.store.isLoading = false;
                this.store.isLoaded = true;
            }
        }, 3000);*/
    }
}

export default new ErrorStore();