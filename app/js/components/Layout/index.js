import {Breadcrumb, Button, Icon, Layout, Menu} from 'antd';
import firebase from 'firebase';
import React, {PureComponent} from 'react';
import {Link, Route} from 'react-router-dom';
import {HOME} from '../../const/MenuConstants';
import Home from '../../pages/Home';
import {redirectToLogin} from '../../utils/ApiUtils';
import './Layout.styl';

const {Header, Content, Footer} = Layout;

class AppLayout extends PureComponent {

    componentWillMount() {
        firebase.auth().onAuthStateChanged(user => {
            if (!user) redirectToLogin();
            console.info('USER', user);
        });
    }

    onLogout = _ => {
        firebase.auth().signOut()
            .then(redirectToLogin)
            .catch(function (err) {
                console.error('LOGOUT ERROR', err);
            });
    };

    render() {
        return (
            <Layout className="error-report-layout">
                <Header>
                    <Link to={HOME}>
                        <div className="logo"/>
                    </Link>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={['1']}
                        style={{lineHeight: '64px'}}
                    >
                        <Menu.Item key="1">Errors Table</Menu.Item>
                    </Menu>
                    <Button ghost onClick={this.onLogout}>
                        <Icon type="logout"/>&nbsp;&nbsp;Logout
                    </Button>
                </Header>
                <Content style={{padding: '0 50px'}}>
                    <Breadcrumb style={{margin: '16px 0'}}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                    </Breadcrumb>
                    <div className="content-wrapper">
                        <Route path={HOME} component={Home} exact/>
                        {/*<Route component={() => <Redirect to={HOME}/>}/>*/}
                    </div>
                </Content>
                {/*<Footer style={{textAlign: 'center'}}>
                    Error Report UI # {__VERSION__} ©2018
                </Footer>*/}
            </Layout>
        );
    }
}

export default AppLayout;

const styles = {
    version: {
        marginTop:      32,
        color:          '#313a4b',
        flex:           1,
        display:        'flex',
        justifyContent: 'center',
        alignItems:     'flex-end',
    },
};
