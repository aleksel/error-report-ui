import keyBy from 'lodash/keyBy';
import {createSelector} from 'reselect';

const channels = state => state.messageChannels.items;

export const channelsMap = createSelector(channels, channels => keyBy(channels, 'id'));