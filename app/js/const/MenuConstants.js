export const URL_PARTS = {
    REQUESTS:                    {
        SOS:      'sos',
        EPD:      'epd',
        FEEDBACK: 'feedback',
        ARCHIVE:  'archive',
    },
    COMMUNICATIONS:              '/communications',
    COMMUNICATION_PARAM:         '/:communication',
    COMMUNICATION_OPTION_PARAM:  '/:option',
    DISTRIBUTION_TEMPLATE_GROUP: '/:group',
    DISTRIBUTION:                '/:distribution',
    DISTRIBUTION_JOURNAL:        '/journal',
    SERVICES:                    {
        GROUP:   '/:group',
        SERVICE: '/:service',
        COMMAND: '/:commandId',
    },

    TELEGRAM: {
        BOT_PARAM:           '/:bot',
        OPTION_PARAM:        '/:option',
        COMMAND_GROUP_PARAM: '/:group',
        COMMAND_PARAM:       '/:command',
        ERROR:               '/errors',
        TASK_PARAM:          '/:taskId',
        TASK_LOGS:           '/logs',
        KEYBOARD_PARAM:      '/:keyboard',

    },
};

export const HOME = '/';

export const USER_PROFILE = '/im';

export const POLLS_ROOT = '/polls';
export const POLLS = POLLS_ROOT + '/edit';
export const POLLS_DASHBOARD = POLLS_ROOT + '/dashboard';
export const POLLS_DASHBOARD_TABLE = POLLS_ROOT + '/dashboard_table';

export const COMMUNICATIONS = URL_PARTS.COMMUNICATIONS;

export const TELEGRAM_BOTS = '/telegram_bots';

export const SERVICES = '/services';

export const REQUESTS = '/requests';
export const SOS_REQUESTS = REQUESTS + '/' + URL_PARTS.REQUESTS.SOS;
export const FEEDBACK_REQUESTS = REQUESTS + '/' + URL_PARTS.REQUESTS.FEEDBACK;
export const EPD_REQUESTS = REQUESTS + '/' + URL_PARTS.REQUESTS.EPD;
export const ARCHIVE_REQUESTS = REQUESTS + '/' + URL_PARTS.REQUESTS.ARCHIVE;

export const NEWS_ROOT = '/news';

export const NEWS_MANAGE = NEWS_ROOT + '/manage';
export const NEWS_DASHBOARD = NEWS_ROOT + '/dashboard';
export const NEWS_CHANGE_HISTORY = NEWS_ROOT + '/history';

export const CHAT_MESSAGES = '/chats';

export const COMPLAINT_ROOT = '/complaints';
export const COMPLAINT_MANAGEMENT = '/complaints/complaint_management';
export const COMPLAINT_DASHBOARD = '/complaints/complaint_dashboard';

export const CITIZENS_ROOT = '/citizens';
export const MOBILE_USERS = '/citizens/mobile_users';
export const CITIZENS_DASHBOARD = '/citizens/dashboard';

export const MEDICINE = '/medicine';
export const DOCTORS = `${MEDICINE}/doctors`;
export const CLINICS = `${MEDICINE}/clinics`;
export const SPECIALITIES = `${MEDICINE}/specialities`;
export const MOBILE_USERS_VACCINATION = `${MEDICINE}/${URL_PARTS.VACCINATION}`;
export const TIME_SLOTS_ROOT = `${MEDICINE}/patients`;
export const TIME_SLOTS_ADD = `${TIME_SLOTS_ROOT}/add`;

export const getClinicTimeSlotPath = (userId = ':userId', slotId = ':slotId') =>
    `${TIME_SLOTS_ROOT}/slot/${userId}/${slotId}`;

//export const COMPLAINT_STATUSES = COMPLAINTS + '/complaint_statuses';
export const SETTINGS_ROOT = '/settings';
export const NEWS_CATEGORIES = `${SETTINGS_ROOT}/categories`;
export const COMPLAINT_STATUSES = `${SETTINGS_ROOT}/complaint_statuses`;
export const COMPLAINTS = `${SETTINGS_ROOT}/complaints`;
export const COMPLAINTS_EDIT_FIELD = `${COMPLAINTS}/edit`;
export const COMPLAINT_STATUS_WORKFLOW = `${SETTINGS_ROOT}/complaint_status_workflow`;
export const OPERATORS = `${SETTINGS_ROOT}/operators`;
export const ROLE_EDITOR = `${SETTINGS_ROOT}/roles_editor`;
export const EVENT_NOTIFICATIONS = `${SETTINGS_ROOT}/event_notifications`;
