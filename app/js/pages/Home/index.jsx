import {DatePicker, Table} from 'antd';
import {observer} from 'mobx-react';
import moment from 'moment';
import * as React from "react";
import errorStore from '../../stores/ErrorStore';
import {DATE_DB_ONLY_FORMAT} from '../../utils/DateUtils';
import './Home.styl';

@observer
export default class Home extends React.Component {

    state = {
        selectedRowKeys: [],
        date:            moment(),
    };

    componentDidMount() {
        errorStore.loadData(this.state.date.format(DATE_DB_ONLY_FORMAT));
    }

    onDateChange = value => this.setState({date: value}, _ => errorStore.loadData(value.format(DATE_DB_ONLY_FORMAT)));

    onTableChange = value => console.log(22222222, value);

    onSelectChange = (selectedRowKeys) => this.setState({selectedRowKeys});

    render() {
        const {selectedRowKeys, date} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        return <div className="error-report-table">
            <div style={styles.datePickerContainer}>

                <div style={styles.datePickerWrapper}>
                    <DatePicker onChange={this.onDateChange} value={date}/>
                </div>
            </div>
            <br/>
            <div>
                <Table
                    onChange={this.onTableChange}
                    loading={errorStore.store.isLoading}
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={errorStore.store.data.slice()}
                />
            </div>
        </div>;
    }
}

const styles = {
    datePickerWrapper:   {float: 'right'},
    datePickerContainer: {display: 'inline-block', width: '100%'},
};

const columns = [{
    title:     'App name',
    dataIndex: 'deviceInfo.bundleId',
}, {
    title:     'Time',
    dataIndex: 'errorTime',
}, {
    title:     'Phone model',
    dataIndex: 'deviceInfo.model',
}];