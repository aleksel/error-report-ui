import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import {WrappedNormalLoginForm} from './Login';
import './../../utils/Firebase';

const rootEl = document.getElementById('login-mount');
const render = (Component, props) => ReactDOM.render(
    <AppContainer>
        <Component {...props} />
    </AppContainer>, rootEl);

render(WrappedNormalLoginForm, {});
if (module.hot) {
    module.hot.accept('./Login', () => {
        render(WrappedNormalLoginForm, {});
        console.log('Hot!');
    });
}
