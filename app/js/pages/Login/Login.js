import {Button, Card, Form, Icon, Input} from 'antd';
import firebase from 'firebase';
import React, {Component} from 'react';
import {HOME} from '../../const/MenuConstants';
import {email} from '../../utils/ValidationUtils';
import './Login.styl';

const FormItem = Form.Item;

class Login extends Component {
    state = {
        loading:       false,
        errorLogin:    null,
        errorPassword: null,
    };

    componentWillReceiveProps(props) {
        const {getFieldDecorator, getFieldsValue} = props.form;
    }

    onFieldsChange = event => {
        console.log(event.target.id);
        if (event.target.id === 'userName') this.setState({errorLogin: null});
        else if (event.target.id === 'password') this.setState({errorPassword: null});
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            let emailValidate = email(values.userName);
            if (emailValidate) {
                this.setState({errorLogin: emailValidate});
                return;
            }
            if (!err) {
                this.setState({loading: true});
                firebase.auth().signInWithEmailAndPassword(values.userName, values.password)
                    .then(data => {
                        this.setState({loading: false});
                        console.log('LOGIN', data);
                        window.location.href = HOME;
                    })
                    .catch(err => {
                        const fieldName = err.message.indexOf('password') !== -1 ? 'errorPassword' : 'errorLogin';
                        this.setState({[fieldName]: err.message, loading: false});
                        console.log('ERROR', err);
                    });
                //putToken(values.token);
                console.log('Received values of form: ', values);
            }
        });
    };

    render() {
        const {getFieldDecorator} = this.props.form;
        const {errorLogin, errorPassword, loading} = this.state;

        return (
            <div id="components-form-normal-login">
                <Card hoverable title="Error Report UI">
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <FormItem
                            validateStatus={errorLogin ? 'error' : undefined}
                            help={errorLogin || undefined}
                        >
                            {getFieldDecorator('userName', {
                                onChange: this.onFieldsChange,
                                rules:    [{required: true, message: 'Please input your username'}],
                            })(
                                <Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                       placeholder="Username"/>,
                            )}
                        </FormItem>
                        <FormItem
                            validateStatus={errorPassword ? 'error' : undefined}
                            help={errorPassword || undefined}
                        >
                            {getFieldDecorator('password', {
                                onChange: this.onFieldsChange,
                                rules: [{
                                    required: true, message: 'Please input your Password'}],
                            })(
                                <Input prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                       type="password"
                                       placeholder="Password"/>,
                            )}
                        </FormItem>
                        <FormItem>
                            <Button
                                loading={loading}
                                disabled={errorLogin || errorPassword}
                                type="primary"
                                htmlType="submit"
                                className="login-form-button"
                            >
                                Let me in
                            </Button>
                        </FormItem>
                    </Form>
                    {/*<FirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()}/>*/}
                </Card>
                <div className="components-form-background-wrapper">
                    <div className="components-form-background"/>
                </div>
            </div>
        );
    }
}

export const WrappedNormalLoginForm = Form.create()(Login);