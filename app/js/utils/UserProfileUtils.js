import store from 'store';
import {USER_TOKEN_HEADER} from './ApiUtils';

export const putToken = payload => {
    store.set(USER_TOKEN_HEADER, payload.token);
};

export const unsetToken = () => new Promise((resolve, reject) => {
    resolve(store.remove(USER_TOKEN_HEADER));
});

export const getToken = () => store.get(USER_TOKEN_HEADER);
