window.addEventListener("focus", event => DesktopNotificationUtils.windowActive = true, false);
window.addEventListener("blur", event => DesktopNotificationUtils.windowActive = false, false);

class DesktopNotificationUtils {

    static tabActive = (() => {
        let stateKey,
            eventKey,
            keys = {
                hidden:       "visibilitychange",
                webkitHidden: "webkitvisibilitychange",
                mozHidden:    "mozvisibilitychange",
                msHidden:     "msvisibilitychange",
            };
        for (stateKey in keys) {
            if (stateKey in document) {
                eventKey = keys[stateKey];
                break;
            }
        }
        return function (c) {
            if (c) document.addEventListener(eventKey, c);
            return !document[stateKey];
        };
    })();

    static windowActive = true;

    static show({onClick, title, message, icon, duration = 5000}) {
        const messageObject = {
            body:               message,
            icon:               icon || require('./../../images/ico.png'),
            requireInteraction: true,
        };
        if ('Notification' in window) {
            if (Notification.permission === 'granted') {
                let notification = new Notification(title, messageObject);
                setTimeout(() => notification.close(), duration);
                notification.onclick = () => {
                    onClick && onClick();
                    window.focus();
                    notification.close();
                };
                return notification;
            } else if (Notification.permission !== 'denied') {
                Notification.requestPermission(function (permission) {
                    if (permission === 'granted') {
                        let notification = new Notification(title, messageObject);
                        setTimeout(() => notification.close(), duration);
                        notification.onclick = () => {
                            onClick && onClick();
                            window.focus();
                            notification.close();
                        };
                        return notification;
                    }
                });
            }
        }
    }
}

DesktopNotificationUtils.isWindowActive = () => DesktopNotificationUtils.windowActive && DesktopNotificationUtils.tabActive();

export default DesktopNotificationUtils;