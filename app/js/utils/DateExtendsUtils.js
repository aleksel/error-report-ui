export const BACKEND_DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSZZ';

Date.prototype.duration = function () {
    var date = this;
    var diff = new Date() - date; // разница в миллисекундах
    if (diff < 1000) { // прошло менее 1 секунды
        return 'a few seconds ago';
    }
    var sec = Math.floor(diff / 1000); // округлить diff до секунд
    if (sec <= 60) {
        return 'a minute ago';
    }
    var min = Math.floor(diff / 60000); // округлить diff до минут
    if (min < 60) {
        return min + ' minutes ago';
    }
    var hours = Math.floor(diff / 3600000); // округлить diff до часов
    if (hours <= 1) {
        return 'an hour ago';
    }
    if (hours <= 22) {
        return hours + ' hours ago';
    }
    var day = Math.floor(diff / 86400000); // округлить diff до дней
    if (day <= 1) {
        return 'a day ago';
    }
    if (day <= 26) {
        return day + ' days ago';
    }
    var month = Math.floor(diff / 2246400000); // округлить diff до месяцев
    if (month <= 1) {
        return 'a month ago';
    }
    if (month <= 11) {
        return month + ' months ago';
    }
    var year = Math.floor(diff / 31104000000); // округлить diff до лет
    if (year <= 1) {
        return 'a year ago';
    }
    return year + ' years ago';
};
Date.prototype.getPastTimeTo = function (dateTo, asString = false) {
    var date = this;
    var seconds = (date.getTime() - dateTo.getTime()) / 1000;
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);

    hours = hours - (days * 24);
    minutes = minutes - (days * 24 * 60) - (hours * 60);
    seconds = Math.floor(seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60));
    if (asString) {
        days = days === 0 ? '' : (days + '');
        hours += '';
        if (hours.length === 1) {
            hours = '0' + hours;
        }
        minutes += '';
        if (minutes.length === 1) {
            minutes = '0' + minutes;
        }
        seconds += '';
        if (seconds.length === 1) {
            seconds = '0' + seconds;
        }
    }
    return {days, hours, minutes, seconds};
};
Date.prototype.formatDate = function (format) {
    var date = this,
        day = date.getDate(),
        timeZone = date.getTimezoneOffset() === 0 ? 0 : Math.round(date.getTimezoneOffset() / 60),
        month = date.getMonth() + 1,
        year = date.getFullYear(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();

    if (!format) {
        format = "MM/dd/yyyy";
    }

    format = format.replace("MM", month.toString().replace(/^(\d)$/, '0$1'));

    if (format.indexOf("yyyy") > -1) {
        format = format.replace("yyyy", year.toString());
    } else if (format.indexOf("yy") > -1) {
        format = format.replace("yy", year.toString().substr(2, 2));
    }

    format = format.replace("dd", day.toString().replace(/^(\d)$/, '0$1'));

    if (format.indexOf("t") > -1) {
        if (hours > 11) {
            format = format.replace("t", "pm");
        } else {
            format = format.replace("t", "am");
        }
    }

    if (format.indexOf("HH") > -1) {
        format = format.replace("HH", hours.toString().replace(/^(\d)$/, '0$1'));
    }

    if (format.indexOf("hh") > -1) {
        if (hours > 12) {
            hours -= 12;
        }

        if (hours === 0) {
            hours = 12;
        }
        format = format.replace("hh", hours.toString().replace(/^(\d)$/, '0$1'));
    }

    if (format.indexOf("mm") > -1) {
        format = format.replace("mm", minutes.toString().replace(/^(\d)$/, '0$1'));
    }

    if (format.indexOf("ss") > -1) {
        format = format.replace("ss", seconds.toString().replace(/^(\d)$/, '0$1'));
    }

    if (format.indexOf("XXX") > -1) {
        var timeZoneCurrent = timeZone < 10 && timeZone > -10 ? ('0' + Math.abs(timeZone) + ":00") : (Math.abs(timeZone) + ":00");
        format = format.replace("XXX", timeZone < 0 ? ('-' + timeZoneCurrent) : ('+' + timeZoneCurrent));
    }

    if (format.indexOf("SSS") > -1) {
        format = format.replace("SSS", '000', '0$1');
    }

    if (format.indexOf("Z") > -1) {
        var timeZoneCurrent = timeZone < 10 && timeZone > -10 ? ('0' + Math.abs(timeZone) + "00") : (Math.abs(timeZone) + "00");
        format = format.replace("Z", timeZone < 0 ? ('-' + timeZoneCurrent) : ('+' + timeZoneCurrent));
    }

    return format;
};