import {Button, notification} from 'antd';
import React from 'react';
import DesktopNotificationUtils from '../utils/DesktopNotificationUtils';

export const showNotification = (title, message, onClickButton, renderButton) => {

    const key = `open${Date.now()}`;
    const onClick = () => {
        onClickButton && onClickButton();
        notification.close(key);
    };

    const config = {
        message:     title,
        description: message,
        duration:    7,
        key,
    };

    const desktopNotify = !DesktopNotificationUtils.isWindowActive() && DesktopNotificationUtils.show({
            onClick,
            title:    title,
            message:  message,
            duration: 7000,
        });

    if (renderButton) {
        config.btn = <Button
            type="primary"
            ghost
            size="small"
            onClick={() => {
                onClick();
                desktopNotify && desktopNotify.close && desktopNotify.close();
            }}
        >
            Go to message
        </Button>;
    }
    notification.info(config);
};