import moment from 'moment';

export const FULL_DATE_FORMAT = 'YYYY-MM-DD, HH:mm:ss';

export const DATE_ONLY_FORMAT = 'YYYY-MM-DD';

export const DATE_DB_ONLY_FORMAT = 'DD-MM-YYYY';

export const YEAR_AND_MONTH = 'YYYY, MMMM(MM)';

export const BACKEND_DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss.SSSZZ';

export const TIME_SHORT = 'HH:mm';

export const formatYearWithMonth = obj => moment(obj).format(YEAR_AND_MONTH);

export const formatForBackend = obj => moment(obj).format(BACKEND_DATE_FORMAT);

export const formatFullDate = obj => moment(obj).format(FULL_DATE_FORMAT);

export const formatDateOnly = obj => moment(obj).format(DATE_ONLY_FORMAT);

export const formatTimeShort = obj => moment(obj).format(TIME_SHORT);
