import firebase from 'firebase';

const firebaseConfig = {
    apiKey:        'AIzaSyDeba0rdah951vzH4jpSkRM99fJP3JSnuo',
    authDomain:    'logs-a14a0.firebaseapp.com',
    databaseURL:   'https://logs-a14a0.firebaseio.com/',
    projectId:     'logs-a14a0',
    storageBucket: 'logs-a14a0.appspot.com',
};
firebase.initializeApp(firebaseConfig);
const database = firebase.database();

export const uiConfig = {
    // Popup signin flow rather than redirect flow.
    signInFlow:       'popup',
    // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
    signInSuccessUrl: (...args) => console.log(11111111, args),
    // We will display Google and Facebook as auth providers.
    signInOptions:    [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        //firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        //firebase.auth.EmailAuthProvider.PROVIDER_ID,
    ],
};

export default class Firebase {
    static getDate = date => {
        return database.ref(date).orderByValue().once('value');
    };
    static setData = (data, appError) => {
        const date = new Date().formatDate('HH:mm:ss');
        const date2 = new Date().formatDate('dd-MM-yyyy');
        database
            .ref(`${date2}/${deviceInfo.bundleId}/${date}${appError ? '-SYSTEM_ERROR' : ''}`.replace(/\./g, '_'))
            .set({error: data, deviceInfo});
    };
}