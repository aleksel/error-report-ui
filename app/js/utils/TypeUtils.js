export const NUMBER_REGEX = /^-?\d+$/;
export const DOUBLE_REGEX = /^-?[0-9]+(\.[0-9]+)?$/;

export default class TypeUtils {
    static formatNumber(number) {
        return number.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace('.00', '');
    }

    static isNumber(number) {
        return NUMBER_REGEX.test(number);
    }

    static isDouble(number) {
        return DOUBLE_REGEX.test(number);
    }

    static isTrue(value, strict = false) {
        if (value instanceof Object) {
            if (!strict || Object.keys(value).length) {
                return true;
            }
            return false;
        }
        if (Array.isArray(value)) {
            if (!strict || value.length) {
                return true;
            }
            return false;
        }
        if (typeof value  === 'string'){
            value = value.toLowerCase();
        }
        switch(value){
            case true:
            case 'true':
            case 1:
            case '1':
            case 'on':
            case 'yes':
                return true;
            case '':
            case 'false':
                return false;
            default:
                return typeof value === 'string';
        }
    }
}