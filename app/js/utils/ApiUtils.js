import axios from 'axios';
import store from 'store';
import firebase from 'firebase';

export const USER_TOKEN_HEADER = 'X-MP-USER-TOKEN';
const HEADERS = {
    'Content-Type':              'application/json',
};

const createOptions = (headers, removeToken = false) => ({
    headers:         getHeaders(headers, removeToken),
    withCredentials: false,
});

export default class ApiUtils {
    static get = (url, headers = {}) => axios.get(url, createOptions(headers));

    static post = (url, data, headers = {}, removeToken = false) => axios
        .post(url, data, createOptions(headers, removeToken));
}

export const redirectToLogin = () => window.location.href = __DEV__ ? '/login.html' : '/login';

const getHeaders = (additionalHeaders = {}, removeToken = false) => {
    let token;
    if (!removeToken && (token = store.get(USER_TOKEN_HEADER))) {
        return {
            ...HEADERS,
            ...additionalHeaders,
            [USER_TOKEN_HEADER]: token,
        };
    } else {
        return {
            ...HEADERS,
            ...additionalHeaders,
        };
    }
};