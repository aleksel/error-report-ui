import DevTools from 'mobx-react-devtools';
import {LocaleProvider} from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import moment from 'moment';
import React, {PureComponent} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Layout from './components/Layout';
import {BACKEND_DATE_FORMAT} from './utils/DateUtils';

moment.fn.toJSON = function () {
    return this.format(BACKEND_DATE_FORMAT);
};

function version() {
    return __VERSION__;
}

window.version = version;

export default class App extends PureComponent {
    render() {
        return (
            <LocaleProvider locale={enUS}>
                <div>
                    {__DEV__ ? <DevTools/> : null}
                    <Router>
                        <Route exact path="*" component={Layout}/>
                    </Router>
                </div>
            </LocaleProvider>
        );
    }
}

