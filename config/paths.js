const fs = require('fs');
const path = require('path');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const paths = {
    publicPath:     '/',
    appSrc:         resolveApp('app/js'),
    appIndexJs:     resolveApp('app/js/index.js'),
    loginIndexJs:   resolveApp('app/js/pages/Login/index.js'),
    appBuild:       resolveApp('build'),
    appHtml:        resolveApp('public/index.html'),
    loginHtml:      resolveApp('public/login.html'),
    appNodeModules: resolveApp('node_modules'),
    appPublic:      resolveApp('public/static'),
};

module.exports = paths;
