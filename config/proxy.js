require('dotenv').config();

const preferMockApi = process.env.PREFER_MOCK_API === 'true';

const proxyTarget = [
    //'http://',
    process.env.PROXY_URL || 'http://127.0.0.1',
    ':',
    process.env.PROXY_PORT || '8899',
    process.env.PROXY_PATH || ''
];

const mockApiTarget = 'http://127.0.0.1:8899';

console.log(
    'Proxying to: ',
    preferMockApi ? mockApiTarget : proxyTarget.join('')
);

module.exports = {
    '/api': {
        target: preferMockApi ? mockApiTarget : proxyTarget.join(''),
        secure: false,
        changeOrigin: true,
        pathRewrite: preferMockApi ? {} : {
            '^/api': '/'
         },
    },
    '/cityservices': {
        target: preferMockApi ? mockApiTarget : proxyTarget.join(''),
        secure: false,
        changeOrigin: true,
        pathRewrite: preferMockApi ? {} : {
            '^/api': '/'
         },
    },
    '/aaa': {
        target: preferMockApi ? mockApiTarget : proxyTarget.join(''),
        secure: false,
        changeOrigin: true,
        pathRewrite: preferMockApi ? {} : {
            '^/aaa': '/'
        },
    },
    '/opi': {
        target: process.env.ADMIN_BACKEND,
        secure: false,
        changeOrigin: true,
        pathRewrite: {
            '^/opi': '/'
        },
    },
};
