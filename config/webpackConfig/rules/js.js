const paths = require('./../../paths');

// Process JS with Babel.

module.exports = {
    test:    /\.(js|jsx)$/,
    include: paths.appSrc,
    loader:  require.resolve('babel-loader'),
    options: {
        'cacheDirectory': true,
    },
};