const webpack = require('webpack');

module.exports = NODE_ENV => new webpack.DefinePlugin({
    NODE_ENV:    JSON.stringify(NODE_ENV || 'prod'),
    PORT:        JSON.stringify(process.env.URL),
    IP:          JSON.stringify(process.env.PORT),
    __DEV__:     NODE_ENV === 'dev',
    __VERSION__: JSON.stringify(require('../../package.json').version),

    API_URL:     JSON.stringify(process.env.PROXY_URL),
    API_PORT:    JSON.stringify(process.env.PROXY_PORT),
});